package com.ibdp.br.asoscomdeus.Networking;

import com.ibdp.br.asoscomdeus.Models.PassagemModel;
import com.ibdp.br.asoscomdeus.Models.PassagensResponse;
import com.ibdp.br.asoscomdeus.Models.VerModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;

/**
 * Created by Hadara on 27/09/2017.
 */

public interface APIService {

    public static final String BASE_URL = "http://asoscomdeus.000webhostapp.com/";

    @GET("passagens.php")
    Call<List<PassagemModel>> getPassagens();

    @GET("v.php")
    Call<VerModel> getVersion();

}
