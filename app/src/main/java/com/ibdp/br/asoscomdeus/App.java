package com.ibdp.br.asoscomdeus;

import android.app.Application;
import android.content.Context;

import com.ibdp.br.asoscomdeus.Networking.APIService;

import org.litepal.LitePal;

/**
 * Created by Hadara on 27/09/2017.
 */

public class App extends Application {
    private static APIService serverAPI;
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        LitePal.initialize(this);
    }

    public static App getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Configuração do AndroidManifest.xml!");
        }
        return instance;
    }

    public static Context getContext(){
        return instance;
    }

    public static APIService getServerAPI() {
        return serverAPI;
    }
}
