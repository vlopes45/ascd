package com.ibdp.br.asoscomdeus.Models;

import org.litepal.crud.DataSupport;

/**
 * Created by Hadara on 03/02/2018.
 */

public class Pregacao extends DataSupport {

    Integer id;

    String pregador;

    String pregacao;

    String passagem;

    String data;

    public Pregacao(String pregador, String pregacao, String passagem, String data) {
        this.pregador = pregador;
        this.pregacao = pregacao;
        this.passagem = passagem;
        this.data = data;
    }

    public String getPregador() {
        return pregador;
    }

    public void setPregador(String pregador) {
        this.pregador = pregador;
    }

    public String getPregacao() {
        return pregacao;
    }

    public void setPregacao(String pregacao) {
        this.pregacao = pregacao;
    }

    public String getPassagem() {
        return passagem;
    }

    public void setPassagem(String passagem) {
        this.passagem = passagem;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getId() {
        return id;
    }
}
