package com.ibdp.br.asoscomdeus;

/**
 * Created by Hadara on 29/10/2017.
 */
        import android.content.Context;
        import android.media.MediaPlayer;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.ImageButton;
        import android.widget.TextView;

        import com.ibdp.br.asoscomdeus.Models.Decisao;
        import com.ibdp.br.asoscomdeus.Views.NotesFragment;

        import java.util.ArrayList;
        import java.util.List;

public class ChatArrayAdapter extends ArrayAdapter<Decisao> {

    private TextView chatText;
    private List<Decisao> chatMessageList = new ArrayList<Decisao>();
    private Context context;
    private NotesFragment Frag;

    @Override
    public void add(Decisao object) {
        chatMessageList.add(object);
        super.add(object);
    }

    public ChatArrayAdapter(Context context, int textViewResourceId,NotesFragment frag) {
        super(context, textViewResourceId);
        this.context = context;
        this.Frag = frag;
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public Decisao getItem(int index) {
        return this.chatMessageList.get(index);
    }

    public View getView(int position, View convertView, final ViewGroup parent) {
        final Decisao chatMessageObj = getItem(position);
        View row = convertView;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(position==0){
            row= inflater.inflate(R.layout.msg_left,parent,false);
        }else {
            row = inflater.inflate(R.layout.msg, parent, false);
        }
        if(chatMessageObj.getDecisao_text().contains("Audio")){
            row = inflater.inflate(R.layout.msgaudio, parent, false);
            final ImageButton play = (ImageButton) row.findViewById(R.id.audio_play);
            final ImageButton pause = (ImageButton) row.findViewById(R.id.audio_pause);
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Frag.playAudio(chatMessageObj.getDecisao_text().split("-")[1]);
                    pause.setVisibility(View.VISIBLE);
                    play.setVisibility(View.GONE);
                    Frag.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            play.setVisibility(View.VISIBLE);
                            pause.setVisibility(View.GONE);
                        }
                    });
                }
            });
            pause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Frag.stopAudio();

                    play.setVisibility(View.VISIBLE);
                    pause.setVisibility(View.GONE);
                }
            });
        }
        row.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                return false;
            }
        });
        chatText = (TextView) row.findViewById(R.id.msgr);
        chatText.setText(chatMessageObj.getDecisao_text());
        return row;
    }
}