package com.ibdp.br.asoscomdeus.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ibdp.br.asoscomdeus.Models.Decisao;
import com.ibdp.br.asoscomdeus.Models.PassagemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hadara on 02/10/2017.
 */

public class DatabaseController {

    private SQLiteDatabase db;
    private SQLiteHelper helper;

    public DatabaseController(Context context){
        helper = new SQLiteHelper(context);
    }

    public String addPassagens(PassagemModel passagem) {
        long resultado;
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", String.valueOf(passagem.getData()));
        values.put("texto", passagem.getTexto());
        values.put("marcacao", passagem.getMarcacao());
        resultado = db.insert("T_PASSAGENS", null, values);
        db.close();
        if (resultado ==-1){
            return "Erro ao inserir registro";}
        else {
            return "Registro Inserido com sucesso";
        }
    }

    public PassagemModel getPassagem(String data){
        Log.d("data",data);
        String selectQuery = "SELECT * FROM 'T_PASSAGENS' WHERE data ='"+data+"'";
        Log.d("dataQuery",selectQuery);
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Log.d("dataQuery", String.valueOf(cursor.getCount()));
        PassagemModel passagem;
        if( cursor != null && cursor.moveToFirst() ) {
            Log.d("dataQdate",cursor.getString(1));
            passagem = new PassagemModel(cursor.getInt(0), cursor.getString(2), cursor.getString(3), cursor.getString(1));
            cursor.close();
            return passagem;
        }else{
            passagem = new PassagemModel(0,"Não há passagem para esse dia","Vazio","Vazio");
            cursor.close();
            return passagem;
        }
//        }if(id==1){
//            passagem = new PassagemModel(1,texto1,marcacao1,data1);
//        }else if(id==2){
//            passagem = new PassagemModel(1,texto2,marcacao2,data2);
//        }else{
//            passagem = new PassagemModel(1,"Vazio","Vazio","Vazio");
//            cursor.close();
//        }
//        return passagem;
        // return contact

    }

    public List<PassagemModel> getPassagens(String mes) {
        List<PassagemModel> passagemList = new ArrayList<PassagemModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM T_PASSAGENS where strftime('%m', data) ="+mes;

        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PassagemModel contact = new PassagemModel(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                // Adding contact to list
                passagemList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return passagemList;
    }

    public void addDecisoes(Decisao decisao) {
        Log.d("dataQaddid", String.valueOf(decisao.getId_passagem()));
        if(decisao.getId_passagem()!=0) {
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("data", String.valueOf(decisao.getData()));
            values.put("id_passagem", decisao.getId_passagem());
            values.put("decisao_text", decisao.getDecisao_text());
            db.insert("T_DECISOES", null, values);
            db.close();
        }
    }

    public void updateDecisoes(Decisao decisao) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", String.valueOf(decisao.getData()));
        values.put("decisao_text", decisao.getDecisao_text());
        db.update("T_DECISOES", values,"id="+decisao.getId(),null);
        db.close();
    }

    public Decisao getDecisao(int id) {
        String selectQuery = "SELECT * FROM 'T_DECISOES' WHERE id_passagem ="+id +" AND id > 0";
        Log.d("dataQuery",selectQuery);
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Log.d("dataQuery", String.valueOf(cursor.getCount()));
        Decisao decisao;
        if( cursor != null && cursor.moveToFirst() ) {
            Log.d("dataQdate",cursor.getString(1));
            decisao = new Decisao(cursor.getInt(0), cursor.getString(2), cursor.getInt(3), cursor.getString(1));
            cursor.close();
            return decisao;
        }else{
            decisao = new Decisao(0,"Não há decisão para esse dia ainda",id,"Vazio");
            cursor.close();
            return decisao;
        }
    }

    public ArrayList<Decisao> getDecisoes(int id) {
        ArrayList<Decisao> decisaoList = new ArrayList<Decisao>();
        // Select All Query
        String selectQuery = "SELECT  * FROM T_DECISOES WHERE id_passagem ="+id +" AND id > 0";

        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Decisao contact = new Decisao(Integer.parseInt(cursor.getString(0)), cursor.getString(2),Integer.parseInt( cursor.getString(3)), cursor.getString(1));
                // Adding contact to list
                decisaoList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        Log.d("dataQLsize", String.valueOf(decisaoList.size()));
        return decisaoList;
    }

    public void addPalavra(Decisao decisao) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("dataQaddid", String.valueOf(decisao.getId_passagem()));
        values.put("data", String.valueOf(decisao.getData()));
        values.put("id_passagem", decisao.getId_passagem());
        values.put("palavra_text", decisao.getDecisao_text());
        db.insert("T_PALAVRAS", null, values);
        db.close();
    }

    public void updatePalavras(Decisao decisao) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", String.valueOf(decisao.getData()));
        values.put("palavra_text", decisao.getDecisao_text());
        db.update("T_PALAVRAS", values,"id="+decisao.getId(),null);
        db.close();
    }

    public Decisao getPalavra(int id) {
        String selectQuery = "SELECT * FROM 'T_PALAVRAS' WHERE id_passagem ="+id;
        Log.d("dataQuery",selectQuery);
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Log.d("dataQuery", String.valueOf(cursor.getCount()));
        Decisao decisao;
        if( cursor != null && cursor.moveToFirst() ) {
            Log.d("dataQdate",cursor.getString(1));
            decisao = new Decisao(cursor.getInt(0), cursor.getString(2), cursor.getInt(3), cursor.getString(1));
            cursor.close();
            return decisao;
        }else{
            decisao = new Decisao(0,"Você ainda não marcou um versículo para esse dia",id,"Vazio");
            cursor.close();
            return decisao;
        }
    }
}
