package com.ibdp.br.asoscomdeus.Models;

import java.util.List;

/**
 * Created by Hadara on 02/10/2017.
 */

public class PassagensResponse {

    public PassagensResponse(List<PassagemModel> response) {
        this.response = response;
    }

    public List<PassagemModel> getResponse() {
        return response;
    }

    public void setResponse(List<PassagemModel> response) {
        this.response = response;
    }

    List<PassagemModel> response;

}
