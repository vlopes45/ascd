package com.ibdp.br.asoscomdeus.Views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ibdp.br.asoscomdeus.FontsOverride;
import com.ibdp.br.asoscomdeus.Models.Pregacao;
import com.ibdp.br.asoscomdeus.Presenters.EditPregPresenter;
import com.ibdp.br.asoscomdeus.R;

import org.litepal.crud.DataSupport;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nucleus.view.NucleusActivity;

/**
 * Created by Hadara on 24/02/2018.
 */

public class EditPregActivity extends NucleusActivity<EditPregPresenter> {
    @BindView(R.id.date_label_preg)
    TextView date_label;
    @BindView(R.id.pastor_label)
    EditText pastor;
    @BindView(R.id.text_label) EditText passagem;
    @BindView(R.id.pregacao) EditText pregacao;
    @BindView(R.id.preg_save)
    Button preg_save;
    @BindView(R.id.preg_back_btn) Button voltar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "FuturaBookBT.ttf");
        setContentView(R.layout.edit_preg_activity);
        ButterKnife.bind(this);
        Integer Id = getIntent().getIntExtra("pregId",1);
        final List<Pregacao> preg = DataSupport.where("id=?", String.valueOf(Id)).find(Pregacao.class);
        date_label.setText(preg.get(0).getData());
        pastor.setText(preg.get(0).getPregador());
        passagem.setText(preg.get(0).getPassagem());
        pregacao.setText(preg.get(0).getPregacao());
        preg_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preg.get(0).setPassagem(passagem.getText().toString());
                preg.get(0).setPregacao(pregacao.getText().toString());
                preg.get(0).setData(date_label.getText().toString());
                preg.get(0).setPregador(pastor.getText().toString());
                preg.get(0).save();
                showMessage("Pragação Salva");
                finish();
            }
        });
        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        date_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(view);
            }
        });

    }
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public void showDatePickerDialog(View v) {
        final Calendar c = Calendar.getInstance();
        int dia = c.get(Calendar.DAY_OF_MONTH);
        int mes = c.get(Calendar.MONTH);
        int ano = c.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date_label.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);

            }
        }
                , dia, mes, ano);
        datePickerDialog.updateDate(ano,mes,dia);
        datePickerDialog.show();
    }


}
