package com.ibdp.br.asoscomdeus.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibdp.br.asoscomdeus.App;
import com.ibdp.br.asoscomdeus.Models.Pregacao;
import com.ibdp.br.asoscomdeus.R;
import com.ibdp.br.asoscomdeus.Views.EditPregActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hadara on 03/02/2018.
 */
public class PregacaoAdapter extends RecyclerView.Adapter<PregacaoAdapter.CardViewHolder> {


    List<Pregacao> Pregs;

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.preg_name) TextView pregname;
        @BindView(R.id.preg_text) TextView pregtext;
        CardViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this,itemView);

        }




    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_preg, parent, false);
        CardViewHolder card = new CardViewHolder(v);
        return card;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, final int position) {
        holder.pregname.setText(Pregs.get(position).getPregador());
        holder.pregtext.setText(Pregs.get(position).getPassagem());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPreg(view.getContext(),Pregs.get(position).getId());
            }
        });
    }

    public PregacaoAdapter(List<Pregacao> Cards) {
        this.Pregs = Cards;
    }

    @Override
    public int getItemCount() {
        return Pregs.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public void editPreg(Context context, Integer id){
        Intent intent = new Intent(context,EditPregActivity.class);
        intent.putExtra("pregId", id);
        context.startActivity(intent);
    }
}

