package com.ibdp.br.asoscomdeus.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hadara on 08/10/2017.
 */

public class VerModel {

    @SerializedName("ver")
    @Expose
    private String ver;

    public VerModel(String ver) {
        this.ver = ver;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }


}
