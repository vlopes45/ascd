package com.ibdp.br.asoscomdeus.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ibdp.br.asoscomdeus.Models.Decisao;
import com.ibdp.br.asoscomdeus.Models.PassagemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hadara on 24/09/2017.
 */

public class SQLiteHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 5;
    public static final String DATABASE_NAME = "ascd.db";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DATA = "data";
    public static final String COLUMN_MARCACAO = "marcacao";
    public static final String COLUMN_TEXT = "texto";
    public static final String COLUMN_ID_PASSAGEM = "id_passagem";
    public static final String COLUMN_PALAVRA_DO_DIA = "palavra_text";
    public static final String COLUMN_DECISAO = "decisao_text";


    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_T_PASSAGENS = "create table T_PASSAGENS ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_DATA + " DATE, " + COLUMN_TEXT + " TEXT, " + COLUMN_MARCACAO + " VARCHAR);";
        String CREATE_T_DECISOES = "create table T_DECISOES ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_DATA + " DATE, " + COLUMN_DECISAO + " TEXT, " + COLUMN_ID_PASSAGEM + " INT);";
        String CREATE_T_PALAVRAS = "create table T_PALAVRAS ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_DATA + " DATE, " + COLUMN_PALAVRA_DO_DIA + " TEXT, " + COLUMN_ID_PASSAGEM + " INT);";
        db.execSQL(CREATE_T_PASSAGENS);
        db.execSQL(CREATE_T_PALAVRAS);
        db.execSQL(CREATE_T_DECISOES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS T_PASSAGENS");
        db.execSQL("DROP TABLE IF EXISTS T_PALAVRAS");
        db.execSQL("DROP TABLE IF EXISTS T_DECISOES");
        onCreate(db);
    }


}
