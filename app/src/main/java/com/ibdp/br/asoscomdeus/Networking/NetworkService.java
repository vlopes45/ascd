package com.ibdp.br.asoscomdeus.Networking;

import android.content.Context;
import android.util.Log;

import com.ibdp.br.asoscomdeus.App;
import com.ibdp.br.asoscomdeus.Models.PassagemModel;
import com.ibdp.br.asoscomdeus.Models.PassagensResponse;
import com.ibdp.br.asoscomdeus.Models.VerModel;
import com.ibdp.br.asoscomdeus.Models.getReq;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hadara on 27/09/2017.
 */

public class NetworkService {

    private Retrofit retrofit;

    public APIService apiService;


    public NetworkService() {


        // OkHttpClient client = new OkHttpClient();
        //OkHttpClient.Builder builder = new OkHttpClient.Builder();

        //builder.addInterceptor(new AddCookiesInterceptor(context)); // VERY VERY IMPORTANT
        // builder.addInterceptor(new ReceivedCookiesInterceptor(context)); // VERY VERY IMPORTANT

        //client = builder.build();
        Context context = App.getContext();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        OkHttpClient client = null;

//builder.addInterceptor(new Interceptor() {
//    @Override
//    public Response intercept(Chain chain) throws IOException {
//        Response response = chain.proceed(chain.request());
//        Log.w("Retrofit@Response", response.body().string());
//        return response;
//    }
//});

        client = builder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        apiService = retrofit.create(APIService.class);

    }
    public void getPassagens(Callback<List<PassagemModel>> callback){
        Call<List<PassagemModel>> call = apiService.getPassagens();
        call.enqueue(callback);
    }

    public void getVersion(Callback<VerModel> callback){
        Call<VerModel> call = apiService.getVersion();
        call.enqueue(callback);
    }

}
