package com.ibdp.br.asoscomdeus.Presenters;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.ibdp.br.asoscomdeus.App;
import com.ibdp.br.asoscomdeus.Models.PassagemModel;
import com.ibdp.br.asoscomdeus.Models.PassagensResponse;
import com.ibdp.br.asoscomdeus.Models.VerModel;
import com.ibdp.br.asoscomdeus.Models.getReq;
import com.ibdp.br.asoscomdeus.Networking.NetworkService;
import com.ibdp.br.asoscomdeus.SQLite.DatabaseController;
import com.ibdp.br.asoscomdeus.SQLite.SQLiteHelper;
import com.ibdp.br.asoscomdeus.Views.MainActivity;

import java.util.List;

import nucleus.presenter.RxPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hadara on 24/09/2017.
 */

public class MainPresenter extends RxPresenter<MainActivity> implements Callback<List<PassagemModel>>{

    private NetworkService service;
    private MainActivity view;
    private String versao;

    public void setView(MainActivity view) {
        this.view = view;
        service = new NetworkService();
    }

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        service = new NetworkService();
    }

    public void Load(){
        Call<VerModel> call = service.apiService.getVersion();
        call.enqueue(new Callback<VerModel>() {
            @Override
            public void onResponse(Call<VerModel> call, Response<VerModel> response) {
                VerModel resposta = response.body();
                SharedPreferences settings = view.getApplicationContext().getSharedPreferences("ascdapp", 0);
                String versaob = settings.getString("versao","");
                Log.d("Ver",resposta.getVer());
                if(!resposta.getVer().equals(versaob)) {
                    versao = resposta.getVer();
                    service.getPassagens(MainPresenter.this);
                }else{
                    view.showMessage("Versão atualizada");
                }
            }
            @Override
            public void onFailure(Call<VerModel> call, Throwable t) {
                view.showMessage("Houve um erro ao atualizar");
            }
        });

    }

    @Override
    public void onResponse(Call<List<PassagemModel>> call, Response<List<PassagemModel>> response) {
        List<PassagemModel> resposta = response.body();
        if(response.code()==200) {
//            Log.d("dead","");
            for (PassagemModel passagem : resposta) {
//                Log.d("Pass: ",passagem.getTexto());
                DatabaseController crud = new DatabaseController(view.getBaseContext());
                String resultado = crud.addPassagens(passagem);
                view.showMessage(resultado);
                SharedPreferences settings = view.getApplicationContext().getSharedPreferences("ascdapp", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("versao", versao);
                editor.apply();
            }
        }else{
            view.showMessage("Houve um erro de rede");
        }

    }

    @Override
    public void onFailure(Call<List<PassagemModel>> call, Throwable t) {
        view.showMessage("Falha ao se comunicar com o servidor");
    }
}
