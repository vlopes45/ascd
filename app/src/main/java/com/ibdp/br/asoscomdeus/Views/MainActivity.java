package com.ibdp.br.asoscomdeus.Views;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ibdp.br.asoscomdeus.App;
import com.ibdp.br.asoscomdeus.FontsOverride;

import com.ibdp.br.asoscomdeus.Models.PassagemModel;
import com.ibdp.br.asoscomdeus.Presenters.MainPresenter;
import com.ibdp.br.asoscomdeus.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import it.sephiroth.android.library.tooltip.Tooltip;
import nucleus.view.NucleusFragmentActivity;

public class MainActivity extends NucleusFragmentActivity<MainPresenter> implements DrawerLayout.OnDragListener, MainFragment.OnFragmentInteractionListener,HelpFragment.OnFragmentInteractionListener,NotesFragment.OnFragmentInteractionListener {

    public static MyPagerAdapter adapterViewPager;
    public static PagerAdapterSingle adapterViewPager2;
    public  LinearLayout leftBartools;
    private MainPresenter presenter = new MainPresenter();
    public static Button menuBtn,helpBtn,ttsBtn,marcarBtn,cfgBtn;
    public static TextView wday_label,marcacao_label,month_label;
    public static Button day_label;
    SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
    Date d = new Date();
    public static Boolean tuton =false;

    String dayOfTheWeek = sdf.format(d);
    public static PassagemModel passagem_hoje,passagem_atual;
    Calendar cal=Calendar.getInstance();
    SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
    String month_name = month_date.format(cal.getTime());

    public static CalendarDay data_atual = CalendarDay.today();
    public static ViewPager vpPager;
    public static boolean locked = true;
    public static void ChangeAdap(int dir){
        if(dir==1){
            vpPager.removeAllViews();
            vpPager.setAdapter(null);
            vpPager.setAdapter(adapterViewPager2);
            vpPager.setCurrentItem(0);
            vpPager.invalidate();
        }else{
            vpPager.removeAllViews();
            vpPager.setAdapter(adapterViewPager);
            vpPager.setCurrentItem(0);
            vpPager.invalidate();
        }
    }

    public  MainActivity GetMe(){
        return this;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "FuturaBookBT.ttf");
        setContentView(R.layout.activity_main);



        ttsBtn = (Button) findViewById(R.id.btn_tts);
        day_label =(Button) findViewById(R.id.day_btn_label);
        day_label.setText(String.valueOf(data_atual.getDay()));
        rightToolTipPopupWindow(day_label);
        wday_label = (TextView) findViewById(R.id.wday_label);
        wday_label.setText(String.valueOf(dayOfTheWeek));
        marcacao_label = (TextView) findViewById(R.id.marcacao_label) ;
        month_label = (TextView) findViewById(R.id.mes_label);
        month_label.setText(String.valueOf(month_name));
        vpPager = (ViewPager) findViewById(R.id.viewPager);

//        presenter.setView(this);
//        presenter.Load();
        leftBartools = (LinearLayout) findViewById(R.id.leftBarTools);

        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager(),data_atual);
        adapterViewPager2 = new PagerAdapterSingle(getSupportFragmentManager());
        menuBtn = (Button) findViewById(R.id.menuBtn);
        menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vpPager.setCurrentItem(0);
            }
        });
        marcarBtn = (Button) findViewById(R.id.btn_marcar);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        cfgBtn = (Button) findViewById(R.id.cfgBtn);
        final LayoutInflater inflater = getLayoutInflater();
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageToast(1,MainActivity.this,inflater);
            }
        });
        cfgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingsToast();
            }
        });
        vpPager.setAdapter(adapterViewPager);
        vpPager.setCurrentItem(0);
        vpPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position)
            {
                if(position==2){
                    marcarBtn.setVisibility(View.GONE);
                }
                if(position==1){
                    leftBartools.setVisibility(View.VISIBLE);
                    menuBtn.setVisibility(View.VISIBLE);
                    helpBtn.setVisibility(View.GONE);
                    marcarBtn.setVisibility(View.VISIBLE);
                    int dia =  data_atual.getDay();
                    int mes =  data_atual.getMonth();
                    int ano =  data_atual.getYear();
                    day_label.setText(String.valueOf(dia));
                    SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                    String dayOfTheWeek = sdf.format(data_atual.getDate());
                    SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
                    String month_name = month_date.format(data_atual.getDate());
                    month_label.setText(String.valueOf(month_name));
                    wday_label.setText(String.valueOf(dayOfTheWeek));
                    marcacao_label.setText(passagem_atual.getMarcacao());
                }
                if(position==0){
                    leftBartools.setVisibility(View.GONE);
                    menuBtn.setVisibility(View.GONE);
                    helpBtn.setVisibility(View.VISIBLE);
                    month_label.setText(String.valueOf(month_name));
                    wday_label.setText(String.valueOf(dayOfTheWeek));
                    day_label.setText(String.valueOf(CalendarDay.today().getDay()));
                    marcacao_label.setText(passagem_hoje.getMarcacao());
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        // assuming mDrawerLayout is an instance of android.support.v4.widget.DrawerLayout
        try {

            // get dragger responsible for the dragging of the left drawer
            Field draggerField = DrawerLayout.class.getDeclaredField("mLeftDragger");
            draggerField.setAccessible(true);
            ViewDragHelper vdh = (ViewDragHelper)draggerField.get(drawer);

            // get access to the private field which defines
            // how far from the edge dragging can start
            Field edgeSizeField = ViewDragHelper.class.getDeclaredField("mEdgeSize");
            edgeSizeField.setAccessible(true);

            // increase the edge size - while x2 should be good enough,
            // try bigger values to easily see the difference
            int origEdgeSize = (int)edgeSizeField.get(vdh);
            int newEdgeSize = (int) (origEdgeSize * 6);
            edgeSizeField.setInt(vdh, newEdgeSize);

        } catch (Exception e) {
            // we unexpectedly failed - e.g. if internal implementation of
            // either ViewDragHelper or DrawerLayout changed
        }

        try {

            // get dragger responsible for the dragging of the left drawer
            Field draggerField = DrawerLayout.class.getDeclaredField("mRightDragger");
            draggerField.setAccessible(true);
            ViewDragHelper vdh = (ViewDragHelper)draggerField.get(drawer);

            // get access to the private field which defines
            // how far from the edge dragging can start
            Field edgeSizeField = ViewDragHelper.class.getDeclaredField("mEdgeSize");
            edgeSizeField.setAccessible(true);

            // increase the edge size - while x2 should be good enough,
            // try bigger values to easily see the difference
            int origEdgeSize = (int)edgeSizeField.get(vdh);
            int newEdgeSize = (int) (origEdgeSize * 12);
            edgeSizeField.setInt(vdh, newEdgeSize);

        } catch (Exception e) {
            // we unexpectedly failed - e.g. if internal implementation of
            // either ViewDragHelper or DrawerLayout changed
        }
        toggle.syncState();

}

    public void rightToolTipPopupWindow(View view) {
        Button toolTipShowButton = (Button) findViewById(R.id.helpBtn);
        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(toolTipShowButton, Tooltip.Gravity.RIGHT)
                        .closePolicy(Tooltip.ClosePolicy.TOUCH_INSIDE_CONSUME, 5000)
                        .activateDelay(900)
                        .withStyleId(R.style.ToolTipLayoutCustomStyleK)
                        .showDelay(300)
                        .text("Clique aqui para ver a ajuda")
                        .maxWidth(300)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        return false;
    }


    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 3;
        private CalendarDay data;

        public MyPagerAdapter(FragmentManager fragmentManager,CalendarDay data_atual) {
            super(fragmentManager);
            data = data_atual;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
                return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return MainFragment.newInstance();
                case 1:
                    return HelpFragment.newInstance(data.getDay(),data.getMonth(),data.getYear());
                case 2:
                    return NotesFragment.newInstance(data.getDay(),data.getMonth(),data.getYear());
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }



    }

    public static class PagerAdapterSingle extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public PagerAdapterSingle(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return HelpFragment.newInstance(data_atual.getDay(),data_atual.getMonth(),data_atual.getYear());
                case 1:
                    return NotesFragment.newInstance(data_atual.getDay(),data_atual.getMonth(),data_atual.getYear());
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (vpPager.getCurrentItem()==1) {
                vpPager.setCurrentItem(0);
            }else if(vpPager.getCurrentItem()==2){
                vpPager.setCurrentItem(1);

            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

        public static void ImageToast(int tutmode, final Context context, LayoutInflater inflater){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        /*builder.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setNegativeButton("Sair do Anúncio", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });*/
        final AlertDialog dialog = builder.create();

        View dialogLayout = inflater.inflate(R.layout.toast_dec, null);
            final ImageView toast_icon = (ImageView) dialogLayout.findViewById(R.id.toast_icon);
            final TextView text_up = (TextView) dialogLayout.findViewById(R.id.toast_view_up);
            final TextView text_down = (TextView) dialogLayout.findViewById(R.id.toast_view_down);
            final Button btn_back = (Button) dialogLayout.findViewById(R.id.btn_dec_back);
            final ImageButton btn_arrow = (ImageButton) dialogLayout.findViewById(R.id.toast_arrow);
        final Button btn_go = (Button) dialogLayout.findViewById(R.id.btn_dec_go);
            if(tutmode == 1) {
                btn_go.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tuton =true;
                        btn_go.setVisibility(View.GONE);
                        btn_back.setVisibility(View.GONE);
                        btn_arrow.setVisibility(View.VISIBLE);
                        text_up.setText("Esta é a tela inicial,nela você terá acesso as meditações de cada dia");
                        text_down.setVisibility(View.GONE);
                        btn_arrow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                text_up.setText("Escolha um dia para fazer a meditação");
                                btn_arrow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                            }
                        });
                    }
                });
            }else if(tutmode==2){
                btn_go.setVisibility(View.GONE);
                btn_back.setVisibility(View.GONE);
                btn_arrow.setVisibility(View.VISIBLE);
                text_up.setText("Aqui você encontrará o texto para meditação");
                text_down.setVisibility(View.GONE);
                btn_arrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        toast_icon.setVisibility(View.VISIBLE);
                        text_up.setText("Você pode ouvir o texto ao clicar");
                        btn_arrow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                toast_icon.setImageResource(R.mipmap.marcar);
                                text_up.setText("Se desejar marcar um versículo que lhe chamou a atenção, clique");
                                btn_arrow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        toast_icon.setImageResource(R.mipmap.menu);
                                        toast_icon.setScaleX(1.0f);
                                        toast_icon.setScaleY(1.0f);
                                        text_up.setText("Para voltar ao menu principal, clique");
                                        btn_arrow.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                dialog.dismiss();
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }});
            }

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

//        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface d) {
//                ImageView image = (ImageView) dialog.findViewById(R.id.image);
//                Bitmap icon = BitmapFactory.decodeResource(MainActivity.this.getResources(),
//                        R.drawable.app_image_popup);
//                float imageWidthInPX = (float)image.getWidth();
//                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Math.round(imageWidthInPX),
//                        Math.round(imageWidthInPX * (float)icon.getHeight() / (float)icon.getWidth()));
//                image.setLayoutParams(layoutParams);
//
//            }
//        });
    }

    public void SettingsToast(){
        final int[] bg_id = {0};
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        bg_id[0] = sharedPref.getInt("bg_id",0);
        final int[] size_idx = {sharedPref.getInt("size_idx", 5)};
        Log.d("BG_ID", String.valueOf(bg_id[0]));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        /*builder.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setNegativeButton("Sair do Anúncio", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });*/
        final AlertDialog dialog = builder.create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.toast_sets, null);
        final SeekBar fontSizeBar = (SeekBar) dialogLayout.findViewById(R.id.fontSizeBar);
        fontSizeBar.setProgress(size_idx[0]);
        final ImageButton bg_1,bg_2,bg_3,bg_0;
        bg_0 = (ImageButton) dialogLayout.findViewById(R.id.bg_0);
        bg_1 = (ImageButton) dialogLayout.findViewById(R.id.bg_1);
        bg_2 = (ImageButton) dialogLayout.findViewById(R.id.bg_2);
        bg_3 = (ImageButton) dialogLayout.findViewById(R.id.bg_3);
        ViewGroup.LayoutParams params_0 = bg_0.getLayoutParams();
        ViewGroup.LayoutParams params_3 = bg_3.getLayoutParams();
        ViewGroup.LayoutParams params_1 = bg_1.getLayoutParams();
        ViewGroup.LayoutParams params_2 = bg_2.getLayoutParams();
        params_2.height = (int) (params_2.height*0.5);
        params_3.height =(int) (params_3.height*0.5);
        params_1.height = (int) (params_1.height*0.5);
        params_0.height = (int) (params_0.height*0.5);
        bg_3.setLayoutParams(params_3);
        bg_2.setLayoutParams(params_2);
        bg_1.setLayoutParams(params_1);
        bg_0.setLayoutParams(params_0);
        ImageButton[] bgs = {bg_0,bg_1,bg_2,bg_3};
        ViewGroup.LayoutParams parambgs = bgs[bg_id[0]].getLayoutParams();
        parambgs.height =  (parambgs.height*3);
        bgs[bg_id[0]].setLayoutParams(parambgs);

        bg_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bg_id[0] !=0) {
                    ViewGroup.LayoutParams params_0 = bg_0.getLayoutParams();
                    ViewGroup.LayoutParams params_3 = bg_3.getLayoutParams();
                    ViewGroup.LayoutParams params_1 = bg_1.getLayoutParams();
                    ViewGroup.LayoutParams params_2 = bg_2.getLayoutParams();
                    params_2.height = 50;
                    params_3.height = 50;
                    params_1.height = 50;
                    bg_3.setLayoutParams(params_3);
                    bg_2.setLayoutParams(params_2);
                    bg_1.setLayoutParams(params_1);
                    params_0.height = 100;
                    bg_0.setLayoutParams(params_0);
                    bg_id[0] =0;
                }
            }
        });
        bg_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bg_id[0] !=1) {
                    ViewGroup.LayoutParams params_0 = bg_0.getLayoutParams();
                    ViewGroup.LayoutParams params_3 = bg_3.getLayoutParams();
                    ViewGroup.LayoutParams params_1 = bg_1.getLayoutParams();
                    ViewGroup.LayoutParams params_2 = bg_2.getLayoutParams();
                    params_0.height =50;
                    params_2.height = 50;
                    params_3.height = 50;
                    bg_0.setLayoutParams(params_0);
                    bg_3.setLayoutParams(params_3);
                    bg_2.setLayoutParams(params_2);
                    params_1.height = 100;
                    bg_1.setLayoutParams(params_1);
                    bg_id[0] =1;
                }
            }
        });
        bg_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bg_id[0] !=2) {
                    ViewGroup.LayoutParams params_0 = bg_0.getLayoutParams();
                    ViewGroup.LayoutParams params_3 = bg_3.getLayoutParams();
                    ViewGroup.LayoutParams params_1 = bg_1.getLayoutParams();
                    ViewGroup.LayoutParams params_2 = bg_2.getLayoutParams();
                    params_0.height =50;
                    params_1.height = 50;
                    params_3.height = 50;
                    bg_0.setLayoutParams(params_0);
                    bg_3.setLayoutParams(params_3);
                    bg_1.setLayoutParams(params_1);
                    params_2.height = 100;
                    bg_2.setLayoutParams(params_2);
                    bg_id[0]=2;
                }
            }
        });
        bg_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bg_id[0] !=3) {
                    ViewGroup.LayoutParams params_0 = bg_0.getLayoutParams();
                    ViewGroup.LayoutParams params_3 = bg_3.getLayoutParams();
                    ViewGroup.LayoutParams params_1 = bg_1.getLayoutParams();
                    ViewGroup.LayoutParams params_2 = bg_2.getLayoutParams();
                    params_0.height = 50;
                    params_1.height = 50;
                    params_2.height = 50;
                    bg_0.setLayoutParams(params_0);
                    bg_2.setLayoutParams(params_2);
                    bg_1.setLayoutParams(params_1);
                    params_3.height = 100;
                    bg_3.setLayoutParams(params_3);
                    bg_id[0] = 3;
                }
            }
        });

        final Button btn_back = (Button) dialogLayout.findViewById(R.id.btn_dec_back);
        final Button btn_go = (Button) dialogLayout.findViewById(R.id.btn_dec_go);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                size_idx[0] = fontSizeBar.getProgress();
                editor.putInt("bg_id", bg_id[0]);
                editor.putInt("size_idx", size_idx[0]);
                editor.commit();
                try {
                    Fragment frag1 = getSupportFragmentManager().getFragments().get(0);
                    Fragment frag2 = getSupportFragmentManager().getFragments().get(1);
                    Fragment frag3 = getSupportFragmentManager().getFragments().get(2);
                    getSupportFragmentManager().beginTransaction().detach(frag1).attach(frag1).commit();
                    getSupportFragmentManager().beginTransaction().detach(frag2).attach(frag2).commit();
                    getSupportFragmentManager().beginTransaction().detach(frag3).attach(frag3).commit();
                }catch (Throwable e) {

                };
                dialog.dismiss();
            }
        });
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

//        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface d) {
//                ImageView image = (ImageView) dialog.findViewById(R.id.image);
//                Bitmap icon = BitmapFactory.decodeResource(MainActivity.this.getResources(),
//                        R.drawable.app_image_popup);
//                float imageWidthInPX = (float)image.getWidth();
//                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Math.round(imageWidthInPX),
//                        Math.round(imageWidthInPX * (float)icon.getHeight() / (float)icon.getWidth()));
//                image.setLayoutParams(layoutParams);
//
//            }
//        });
    }

}
