package com.ibdp.br.asoscomdeus.Models;

import java.util.Date;

/**
 * Created by Hadara on 24/09/2017.
 */

public class PassagemModel {

    int id;
    String texto;
    String marcacao;
    String data;

    public PassagemModel(int id, String texto, String marcacao, String data) {
        this.id = id;
        this.texto = texto;
        this.marcacao = marcacao;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getMarcacao() {
        return marcacao;
    }

    public void setMarcacao(String marcacao) {
        this.marcacao = marcacao;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }



}
