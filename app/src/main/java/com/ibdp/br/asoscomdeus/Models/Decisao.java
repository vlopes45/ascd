package com.ibdp.br.asoscomdeus.Models;

/**
 * Created by Hadara on 24/09/2017.
 */

public class Decisao {

    int id;
    String decisao_text;
    int id_passagem;

    public Decisao(int id, String decisao_text, int id_passagem, String data) {
        this.id = id;
        this.decisao_text = decisao_text;
        this.id_passagem = id_passagem;
        this.data = data;
    }

    String data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDecisao_text() {
        return decisao_text;
    }

    public void setDecisao_text(String decisao_text) {
        this.decisao_text = decisao_text;
    }

    public int getId_passagem() {
        return id_passagem;
    }

    public void setId_passagem(int id_passagem) {
        this.id_passagem = id_passagem;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }





}
