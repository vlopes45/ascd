package com.ibdp.br.asoscomdeus.Views;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ibdp.br.asoscomdeus.App;
import com.ibdp.br.asoscomdeus.Models.Decisao;
import com.ibdp.br.asoscomdeus.Models.PassagemModel;
import com.ibdp.br.asoscomdeus.R;
import com.ibdp.br.asoscomdeus.SQLite.DatabaseController;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import com.ibdp.br.asoscomdeus.ChatArrayAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NotesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NotesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int dia;
    private int mes;
    private int ano;
    private int dec_id;
    public PassagemModel passagem;
    public TextView decisao;
    public Decisao decisaoAtual;
    MediaRecorder myAudioRecorder = new MediaRecorder();
    public MediaPlayer mediaPlayer ;
    String AudioSavePathInDevice = null;
    Random random ;
    String AudioName;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    public static final int RequestPermissionCode = 102;
    private EditText chatText;
    private ImageButton buttonSend,buttonRec;

    private OnFragmentInteractionListener mListener;

    public NotesFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static NotesFragment newInstance(int dia,int mes,int ano) {
        NotesFragment fragment = new NotesFragment();
        Bundle args = new Bundle();
        args.putInt("dia", dia);
        args.putInt("mes", mes);
        args.putInt("ano", ano);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dia = getArguments().getInt("dia");
            mes = getArguments().getInt("mes");
            ano = getArguments().getInt("ano");
        }
        random = new Random();
    }

    public void playAudio(String path){
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +path);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mediaPlayer.start();
        Toast.makeText(getContext(), "Tocando audio",
                Toast.LENGTH_LONG).show();
    }

    public void stopAudio(){
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            MediaRecorderReady();
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notes, container, false);
//        decisao = (TextView) view.findViewById(R.id.texto_dec_ler);
//        decisao.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ImageToast();
//            }
//        });


        HelpFragment helpfrag = (HelpFragment) getFragmentManager().getFragments().get(1);
        passagem = helpfrag.getPassagem();
        DatabaseController crud = new DatabaseController(getActivity().getBaseContext());
        Decisao decisao_crud = crud.getDecisao(passagem.getId());
        decisaoAtual = decisao_crud;
//        decisao.setText(decisao_crud.getDecisao_text());

        listView = (ListView) view.findViewById(R.id.msgView);

        chatArrayAdapter = new ChatArrayAdapter(getActivity().getBaseContext(), R.layout.msg,this);
        Decisao palavra = crud.getPalavra(passagem.getId());
        ArrayList<Decisao> decisoes = new ArrayList<Decisao>();
        decisoes.add(palavra);
        decisoes.addAll(crud.getDecisoes(passagem.getId()));
        Log.d("dataQdecisoesL", String.valueOf(decisoes.size()));
        for(Decisao dec : decisoes){
            chatArrayAdapter.add(dec);
        }
        listView.setAdapter(chatArrayAdapter);

        chatText = (EditText) view.findViewById(R.id.msg);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                    return sendChatMessage();
                }
                return false;
            }
        });
        buttonSend = (ImageButton) view.findViewById(R.id.send);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendmessage();
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        buttonRec = (ImageButton) view.findViewById(R.id.rec_btn);
        buttonRec.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN && passagem.getId()!=0) {
                    buttonRec.setPivotX(buttonRec.getWidth());
                    buttonRec.setPivotY(buttonRec.getHeight());
                    buttonRec.setScaleX(2.3f);
                    buttonRec.setScaleY(2.3f);
                    if(checkPermission()) {
                        AudioName = CreateRandomAudioFileName(5) + "ASCD.3gp";
                        AudioSavePathInDevice =
                                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                        AudioName;

                        MediaRecorderReady();

                        try {
                            myAudioRecorder.prepare();
                            myAudioRecorder.start();
                        } catch (IllegalStateException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        Toast.makeText(getActivity(), "Gravando",
                                Toast.LENGTH_LONG).show();
                    } else {
                        requestPermission();
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP && passagem.getId()!=0) {
                    buttonRec.setScaleX(1);
                    buttonRec.setScaleY(1);
                    if(checkPermission()) {
                        try {
                            myAudioRecorder.stop();
                            recEnd(AudioName);
                            Toast.makeText(getActivity(), "Gravado",
                                    Toast.LENGTH_LONG).show();
                        }catch (Exception e) {

                        }
                    } else {
                        requestPermission();
                    }
                }
                return false;
            }
        });

        EditText msg = (EditText) view.findViewById(R.id.msg);
        msg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==0){
                    buttonRec.setVisibility(View.VISIBLE);
                    buttonSend.setVisibility(View.GONE);
                }else{
                    buttonSend.setVisibility(View.VISIBLE);
                    buttonRec.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        int bg_id = 0;
        Log.d("BG_ID", String.valueOf(bg_id));
        bg_id = sharedPref.getInt("bg_id",bg_id);
        if(bg_id==1){
            view.setBackground(getActivity().getDrawable(R.drawable.back_1));
        }
        else if(bg_id==2){
            view.setBackground(getActivity().getDrawable(R.drawable.back_2));
        }
        else if(bg_id==3){
            view.setBackground(getActivity().getDrawable(R.drawable.back_3));
        }

        return view;
    }

    public void MediaRecorderReady(){
        myAudioRecorder=new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(AudioSavePathInDevice);
    }

    public String CreateRandomAudioFileName(int string){
        StringBuilder stringBuilder = new StringBuilder( string );
        int i = 0 ;
        while(i < string ) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++ ;
        }
        return stringBuilder.toString();
    }

    public void setNotes(int id){
        DatabaseController crud = new DatabaseController(getActivity().getBaseContext());
        chatArrayAdapter = new ChatArrayAdapter(getActivity().getBaseContext(), R.layout.msg,this);
        chatArrayAdapter.clear();
        listView.setAdapter(chatArrayAdapter);
        Decisao palavra = crud.getPalavra(id);
        ArrayList<Decisao> decisoes = new ArrayList<Decisao>();
        decisoes.add(palavra);
        decisoes.addAll(crud.getDecisoes(id));
        Log.d("dataQdecisoesL", String.valueOf(decisoes.size()));
        for(Decisao dec : decisoes){
            chatArrayAdapter.add(dec);
        }
        chatArrayAdapter.notifyDataSetChanged();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void sendmessage(){
        EditText msg = (EditText) getView().findViewById(R.id.msg);
        HelpFragment helpfrag = (HelpFragment) getFragmentManager().getFragments().get(1);
        passagem = helpfrag.getPassagem();
        if(passagem.getId()!=0) {
            Log.d("dataQdecID", String.valueOf(decisaoAtual.getId()));
            DatabaseController crud = new DatabaseController(getActivity().getBaseContext());
            Log.d("dataQPassagem", String.valueOf(passagem.getId()));
            crud.addDecisoes(new Decisao(0, msg.getText().toString(), passagem.getId(), CalendarDay.today().toString()));
            setNotes(passagem.getId());
        }
        msg.setText("");
    }

    private void recEnd(String AudioName){
        HelpFragment helpfrag = (HelpFragment) getFragmentManager().getFragments().get(1);
        passagem = helpfrag.getPassagem();
        if(passagem.getId()!=0) {
            Log.d("dataQdecID", String.valueOf(decisaoAtual.getId()));
            DatabaseController crud = new DatabaseController(getActivity().getBaseContext());
            Log.d("dataQPassagem", String.valueOf(passagem.getId()));
            crud.addDecisoes(new Decisao(-1, "Audio-" + AudioName, passagem.getId(), CalendarDay.today().toString()));
            setNotes(passagem.getId());
        }
    }



    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length> 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(getActivity(), "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(),"Permission Denied",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }
}
