package com.ibdp.br.asoscomdeus.Views;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.ibdp.br.asoscomdeus.Adapters.PregacaoAdapter;
import com.ibdp.br.asoscomdeus.App;
import com.ibdp.br.asoscomdeus.CircleDecorator;
import com.ibdp.br.asoscomdeus.Models.Pregacao;
import com.ibdp.br.asoscomdeus.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarUtils;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.format.CalendarWeekDayFormatter;

import org.litepal.crud.DataSupport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment implements OnMonthChangedListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static MainActivity mainc;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static List<Pregacao> pregs;
    private static PregacaoAdapter adapter;


    @BindView(R.id.rv) RecyclerView rv;

    private OnFragmentInteractionListener mListener;

    public MainFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static void SetMain(MainActivity main){
        mainc = main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        CalendarDay today = CalendarDay.today();

        final MaterialCalendarView cView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        cView.state().edit().setMinimumDate(CalendarDay.from(2017,9,1)).commit();
        cView.state().edit().setMaximumDate(CalendarDay.from(today.getYear(),today.getMonth(),Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH))).commit();
        cView.addDecorator(new CircleDecorator(getActivity().getApplicationContext(),R.drawable.daycolor));
        cView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                mainc.data_atual = date;
                int dia =  date.getDay();
                int mes =  date.getMonth();
                int ano =  date.getYear();
                mainc.day_label.setText(String.valueOf(dia));
//                mainc.wday_label.setText(String.valueOf(CalendarUtils.getDayOfWeek(date.getCalendar())));

                HelpFragment helpfrag = (HelpFragment) getFragmentManager().getFragments().get(1);
                helpfrag.setPassagem(String.valueOf(ano)+"-"+ String.format("%02d",mes+1)+"-"+ String.format("%02d",dia));
                mainc.marcacao_label.setText(helpfrag.getPassagem().getMarcacao());
                mainc.passagem_atual = helpfrag.getPassagem();
                mainc.vpPager.setCurrentItem(1);
                NotesFragment notefrag = (NotesFragment) getFragmentManager().getFragments().get(2);

                notefrag.setNotes(helpfrag.getPassagem().getId());

                SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                String dayOfTheWeek = sdf.format(date.getDate());
                SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
                String month_name = month_date.format(date.getDate());
                mainc.month_label.setText(String.valueOf(month_name));
                mainc.wday_label.setText(String.valueOf(dayOfTheWeek));
                if(mainc.tuton) {
                    mainc.ImageToast(2,MainFragment.this.getContext(),inflater);
                }
            }
        });
        cView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        int bg_id = 0;
        Log.d("BG_ID", String.valueOf(bg_id));
        bg_id = sharedPref.getInt("bg_id",bg_id);
        if(bg_id==1){
            view.setBackground(getActivity().getDrawable(R.drawable.back_1));
        }
        else if(bg_id==2){
            view.setBackground(getActivity().getDrawable(R.drawable.back_2));
        }
        else if(bg_id==3){
            view.setBackground(getActivity().getDrawable(R.drawable.back_3));
        }

        Button addPreg = (Button) view.findViewById(R.id.preg_btn_add);
        addPreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PregToast(inflater.getContext(),inflater);
            }
        });

        ButterKnife.bind(this, view);
        LinearLayoutManager llm = new LinearLayoutManager(inflater.getContext());

        List<Pregacao> pregs_list = DataSupport.findAll(Pregacao.class);
        pregs = new ArrayList<>();
        pregs.addAll(pregs_list);
        adapter = new PregacaoAdapter(pregs);
        rv.setLayoutManager(llm);
        rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
       // widget.state().edit().setMinimumDate(CalendarDay.from(2017,9,1)).commit();
       // widget.addDecorator(new CircleDecorator(getActivity().getApplicationContext(),R.drawable.daycolor,days));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void PregToast(final Context context, LayoutInflater inflater){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        /*builder.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setNegativeButton("Sair do Anúncio", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });*/
        final AlertDialog dialog = builder.create();
        View view = inflater.inflate(R.layout.toast_preg, null);

        final EditText pregador = (EditText) view.findViewById(R.id.editText);
        final EditText texto = (EditText) view.findViewById(R.id.editText3);
        final TextView data = (TextView) view.findViewById(R.id.editText4);
        Button adicionar = (Button) view.findViewById(R.id.button2);
        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Pregacao pregacao = new Pregacao(pregador.getText().toString(),"",texto.getText().toString(),data.getText().toString());
                pregacao.save();
                pregs.add(pregacao);
                adapter.notifyDataSetChanged();
                dialog.dismiss();
                Intent intent = new Intent(context,EditPregActivity.class);
                intent.putExtra("pregId", pregacao.getId());
                startActivity(intent);
            }
        });
        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(view,data);
            }
        });
        dialog.setView(view);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();


    }

    public void editPreg(Integer id){
        Intent intent = new Intent(App.getContext(),EditPregActivity.class);
        intent.putExtra("pregId", id);
        startActivity(intent);
    }

    public void showDatePickerDialog(View v, final TextView label) {
        final Calendar c = Calendar.getInstance();
        int dia = c.get(Calendar.DAY_OF_MONTH);
        int mes = c.get(Calendar.MONTH);
        int ano = c.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                label.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);

            }
        }
                , dia, mes, ano);
        datePickerDialog.updateDate(ano,mes,dia);
        datePickerDialog.show();
    }

}
