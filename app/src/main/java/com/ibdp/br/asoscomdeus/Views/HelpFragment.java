package com.ibdp.br.asoscomdeus.Views;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ibdp.br.asoscomdeus.App;
import com.ibdp.br.asoscomdeus.Models.Decisao;
import com.ibdp.br.asoscomdeus.Models.PassagemModel;
import com.ibdp.br.asoscomdeus.R;
import com.ibdp.br.asoscomdeus.SQLite.DatabaseController;
import com.ibdp.br.asoscomdeus.SQLite.SQLiteHelper;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.util.Locale;

import static com.ibdp.br.asoscomdeus.Views.MainFragment.mainc;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HelpFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HelpFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelpFragment extends Fragment implements TextToSpeech.OnInitListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int dia;
    private int mes;
    private int ano;
    private static TextView texto,marcacao;
    public static PassagemModel passagemAtual;
    private TextToSpeech tts;

    private OnFragmentInteractionListener mListener;

    public HelpFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static HelpFragment newInstance(int dia,int mes,int ano) {
        HelpFragment fragment = new HelpFragment();
        Bundle args = new Bundle();
        args.putInt("dia", dia);
        args.putInt("mes", mes);
        args.putInt("ano", ano);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tts = new TextToSpeech(getContext(), this,"com.google.android.tts");
        if (getArguments() != null) {
            dia = getArguments().getInt("dia");
            mes = getArguments().getInt("mes");
            ano = getArguments().getInt("ano");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final DatabaseController crud = new DatabaseController(getActivity().getBaseContext());
        View view =  inflater.inflate(R.layout.fragment_help, container, false);
        String dia = String.format("%02d",CalendarDay.today().getDay());
        String mes = String.format("%02d",CalendarDay.today().getMonth()+1) ;
        PassagemModel passagem = crud.getPassagem(String.valueOf(CalendarDay.today().getYear())+"-"+mes+"-"+dia);
        texto = (TextView) view.findViewById(R.id.texto_passagem);
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        int size_idx = 5;
        size_idx = sharedPref.getInt("size_idx",size_idx);
        Log.d("SIZE_IDX", String.valueOf(size_idx));
        float fsize = 14.0f*(size_idx/5.0f)+6.0f;
        texto.setTextSize(fsize);
//        marcacao = (TextView) view.getRootView().findViewById(R.id.marcacao_label);
//        marcacao.setText(passagem.getMarcacao());
        passagemAtual = passagem;
        mainc.passagem_hoje = passagem;
        mainc.passagem_atual = passagem;
        mainc.marcacao_label.setText(passagem.getMarcacao());
        mainc.marcarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Decisao palavra = crud.getPalavra(passagemAtual.getId());
                if(palavra.getId()==0) {
                    crud.addPalavra(new Decisao(0, getSelectedText(), passagemAtual.getId(), CalendarDay.today().toString()));
                }else{
                    crud.updatePalavras(new Decisao(palavra.getId(), getSelectedText(), passagemAtual.getId(), CalendarDay.today().toString()));
                }
                Toast.makeText(getContext(), "Versiculo salvo",
                        Toast.LENGTH_LONG).show();
                NotesFragment notefrag = (NotesFragment) getFragmentManager().getFragments().get(2);
                notefrag.setNotes(passagemAtual.getId());
            }
        });
        texto.setText(passagem.getTexto());
        return view;
    }

    public void setPassagem(String data){
        DatabaseController crud = new DatabaseController(getActivity().getBaseContext());
        PassagemModel passagem = crud.getPassagem(data);
        mainc.passagem_atual = passagem;
        passagemAtual = passagem;
        texto.setText(passagem.getTexto());
//        marcacao.setText(passagem.getMarcacao());
    }

    public PassagemModel getPassagem(){
        return passagemAtual;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDetach();
    }

    @Override
    public void onInit(int i) {

        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(new Locale("pt","br"));

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                //mainc.ttsBtn.setEnabled(true);
                //speakOut();
                mainc.ttsBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tts.isSpeaking()) {
                            tts.stop();
                        } else{
                            speakOut();
                    }
                    }
                });
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    private void speakOut() {

        String text = texto.getText().toString();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public String getSelectedText(){
        TextView et=(TextView) getView().findViewById(R.id.texto_passagem);

        int startSelection=et.getSelectionStart();
        int endSelection=et.getSelectionEnd();

        String selectedText = et.getText().toString().substring(startSelection, endSelection);

        return selectedText;
    }
}
